package mis.entregables.lab1.consumorestlab1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Consumorestlab1Application {

	public static void main(String[] args) {
		SpringApplication.run(Consumorestlab1Application.class, args);
	}

}
