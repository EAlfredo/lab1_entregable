package mis.entregables.lab1.consumorestlab1.controlador;

import mis.entregables.lab1.consumorestlab1.modelo.Precio;
import mis.entregables.lab1.consumorestlab1.modelo.Producto;
import mis.entregables.lab1.consumorestlab1.servicios.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
// lab con ResponseEntity
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ContraladorProducto {

    @Autowired
    private ProductoServicio productoServicio;

    // LAB1 Caso 1
    // GET todos los productos
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productoServicio.getProductos();
    }

    // GET PRODUCTO POR ID
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    // LAB1 Caso 2
    // POST
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto producto) {
        productoServicio.addProducto(producto);
        return new ResponseEntity<>("Product created successfully controlando respuesta HTTP!", HttpStatus.CREATED);
    }

    //LAB1 Caso 3
    // ahora le toca al PUT controlando si un mensaje de repuesta 200 con el http .OK
    // OJO con el index de esta ejemplo en la lita inicial solo hay 5 registros
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoServicio.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }

    // LAB1 caso 4
    // AHORA metodo delete
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        Producto pr = productoServicio.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado al querer borralo.", HttpStatus.NOT_FOUND);
        }
        productoServicio.removeProducto(id - 1);
        return new ResponseEntity<>("Este si lo borro", HttpStatus.NO_CONTENT); // status code 204-No Content
    }

    // LAB1 caso 5
    // AHORA metodo patch
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecio(
            @RequestBody Precio precio, @PathVariable int id){
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(precio.getPrecio());
        productoServicio.updateProducto(id-1, pr);
        //return new ResponseEntity<>("Producto no encontrado.", pr, HttpStatus.OK);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    //LAB 1 caso 6, ultimo ejercicio agrega usuario al producto o subproducto en relidad
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado en el subproducto.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsuarios()!=null)
            return ResponseEntity.ok(pr.getUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
