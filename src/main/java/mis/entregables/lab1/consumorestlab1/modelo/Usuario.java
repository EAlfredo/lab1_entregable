package mis.entregables.lab1.consumorestlab1.modelo;

public class Usuario {

    private String userId;

    public Usuario(){}

    public Usuario(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return this.userId;
    }
}
