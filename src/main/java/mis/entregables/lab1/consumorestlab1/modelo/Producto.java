package mis.entregables.lab1.consumorestlab1.modelo;

import java.util.ArrayList;
import java.util.List;

public class Producto {

    private long id;
    private String descripcion;
    private double precio;
    //final List<Integer> idsProveedores = new ArrayList<>();
    private List<Usuario> usuarios;

    public Producto(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.usuarios = new ArrayList<Usuario>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

}
